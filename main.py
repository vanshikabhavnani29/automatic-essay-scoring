# importing flask module
from flask import Flask
from flask import render_template





# initializing a variable of Flask
app = Flask(__name__)


# decorating index function with the app.route
@app.route('/')


def mainpage():
   return render_template('mainpage.html')

if __name__ == "__main__":
   app.run()
